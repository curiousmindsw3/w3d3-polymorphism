#pragma once
#include "QualityUpdater.h"
class BackstageUpdater :
	public QualityUpdater
{
public:
	BackstageUpdater();
	~BackstageUpdater();
	void operator() (Item & item) const override;
};

