#pragma once
#include "QualityUpdater.h"
class SulfurasUpdater :
	public QualityUpdater
{
public:
	SulfurasUpdater();
	~SulfurasUpdater();
	void operator () (Item& item) const override;
};

