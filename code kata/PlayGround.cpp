#include "DoNotModify.h"
#include "DefaultUpdater.h"
#include "AgedBrieUpdater.h"
#include "SulfurasUpdater.h"
#include "ConjuredUpdater.h"
#include "BackstageUpdater.h"

enum class ItemType
{
    DEFAULT,
    AGED_BRIE,
    SULFURAS,
    BACKSTAGE_PASS,
	CONJURED
};

static ItemType GetType(const Item& item)
{
    if (item.GetName() == "Aged Brie")
    {
        return ItemType::AGED_BRIE;
    }

    if (item.GetName() == "Backstage passes to a TAFKAL80ETC concert")
    {
        return ItemType::BACKSTAGE_PASS;
    }

    if (item.GetName() == "Sulfuras, Hand of Ragnaros")
    {
        return ItemType::SULFURAS;
    }

	if (item.GetName() == "Conjured Mana Cake")
	{
		return ItemType::CONJURED;
	}

    return ItemType::DEFAULT;
}

void DecreaseQuality(Item &item)
{
    if (item.GetQuality() > 0)
    {
        item.Update(item.GetSellIn(), item.GetQuality() - 1);
    }
}

void IncreaseQuality(Item &item)
{
    if (item.GetQuality() < 50)
    {
        item.Update(item.GetSellIn(), item.GetQuality() + 1);
    }
}


static void UpdateQuality(Item &item)
{
	switch (GetType(item))
	{
	case ItemType::AGED_BRIE:
	{
		AgedBrieUpdater updater;
		updater(item);
		break;
	}
	case ItemType::BACKSTAGE_PASS:
	{
		BackstageUpdater updater;
		updater(item);
		break;
	}
	case ItemType::CONJURED:
	{
		ConjuredUpdater updater;
		updater(item);
		break;
	}
	case ItemType::DEFAULT:
	{
		DefaultUpdater updater;
		updater(item);
		break;
	}
	case ItemType::SULFURAS: {
		SulfurasUpdater updater;
		updater(item);
		break;
	}

	}
}

void UpdateSellIn(Item &item)
{
    if (GetType(item) != ItemType::SULFURAS)
    {
        item.Update(item.GetSellIn() - 1, item.GetQuality());
    }
}

void UpdatePostSellInQuality(Item &item)
{
    if (item.GetSellIn() < 0)
    {
        switch (GetType(item))
        {
        case ItemType::AGED_BRIE:
            IncreaseQuality(item);
            break;
        case ItemType::BACKSTAGE_PASS:
            item.Update(item.GetSellIn(), 0);
            break;
        case ItemType::DEFAULT:
            DecreaseQuality(item);
            break;
        }
    }
}

void UpdateItem(Item& item)
{
    UpdateQuality(item);
    UpdateSellIn(item);
    UpdatePostSellInQuality(item);
}
