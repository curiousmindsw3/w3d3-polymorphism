#pragma once
#include "DoNotModify.h"
class QualityUpdater
{
public:
	QualityUpdater();
	~QualityUpdater();
	//virtual void UpdateQuality(Item& item) const = 0;
	virtual void operator () (Item& item)const  = 0;
};

