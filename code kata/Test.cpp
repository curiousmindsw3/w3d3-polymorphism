#include "DoNotModify.h"
#include <assert.h>
#include <iostream>

namespace
{
    using ExpectedData = std::pair<Item::DayCount, Item::Quality>;

    template <class T,
        typename value_type = T::value_type,
        typename = std::enable_if_t<std::is_same<ExpectedData, value_type>::value>>
    static void TestItem(const char* name, Item::DayCount sellIn, Item::Quality quality,
        const T& expectedData)
    {
        std::cout << "Testing " << name << ", " << sellIn << ", " << quality
            << " for " << expectedData.size() << " days" << std::endl;

        Shop shop;
        shop.AddItem(name, sellIn, quality);

        Item::DayCount day = 1;
        for (const auto& expected : expectedData)
        {
            shop.Update();
            const Item& item = shop.Items().back();

            std::cout << "\tDay " << day++ << " ";
            if (expected == std::make_pair(item.GetSellIn(), item.GetQuality()))
            {
                std::cout << "PASS" << std::endl;
				//std::cout << "\t\tExpected : " << expected.first << ", " << expected.second << std::endl;
				//std::cout << "\t\tActual   : " << item.GetSellIn() << ", " << item.GetQuality() << std::endl;
            }
            else
            {
                std::cout << "FAIL:" << std::endl;
                std::cout << "\t\tExpected : " << expected.first << ", " << expected.second << std::endl;
                std::cout << "\t\tActual   : " << item.GetSellIn() << ", " << item.GetQuality() << std::endl;
            }
        }
    }

    static void TestItem(const char* name, Item::DayCount sellIn, Item::Quality quality,
        std::initializer_list<ExpectedData> expectedData)
    {
        return TestItem<decltype(expectedData)>(name, sellIn, quality, expectedData);
    }
}

int main()
{
    TestItem("Common product", 1, 4, {
        std::make_pair(0, 3),
        std::make_pair(-1, 1),
        std::make_pair(-2, 0),
        std::make_pair(-3, 0), 
    });

    TestItem("Sulfuras, Hand of Ragnaros", 0, 80, {
        std::make_pair(0, 80) 
    });

    TestItem("Sulfuras, Hand of Ragnaros", -1, 80, {
        std::make_pair(-1, 80) 
    });

    TestItem("Aged Brie", 1, 1, {
        std::make_pair(0, 2),
        std::make_pair(-1, 4),
    });

    TestItem("Aged Brie", 1, 50, {
        std::make_pair(0, 50),
        std::make_pair(-1, 50),
    });

    TestItem("Backstage passes to a TAFKAL80ETC concert", 11, 0, {
        std::make_pair(10, 1),
        std::make_pair(9, 3),
        std::make_pair(8, 5),
        std::make_pair(7, 7),
        std::make_pair(6, 9),
        std::make_pair(5, 11),
        std::make_pair(4, 14),
        std::make_pair(3, 17),
        std::make_pair(2, 20),
        std::make_pair(1, 23),
        std::make_pair(0, 26),
        std::make_pair(-1, 0),
        std::make_pair(-2, 0),
    });

    TestItem("Backstage passes to a TAFKAL80ETC concert", 11, 49, {
        std::make_pair(10, 50),
        std::make_pair(9, 50),
    });

    TestItem("Backstage passes to a TAFKAL80ETC concert", 10, 49, {
        std::make_pair(9, 50),
    });

    TestItem("Backstage passes to a TAFKAL80ETC concert", 5, 49, {
        std::make_pair(4, 50),
    });

    TestItem("Backstage passes to a TAFKAL80ETC concert", 5, 48, {
        std::make_pair(4, 50),
    });

    TestItem("Conjured Mana Cake", 1, 6, {
        std::make_pair(0, 4),
        std::make_pair(-1, 0),
        std::make_pair(-2, 0),
    });

    TestItem("Conjured Mana Cake", 1, 1, {
        std::make_pair(0, 0),
    });

    TestItem("Conjured Mana Cake", 0, 3, {
        std::make_pair(-1, 0),
    });

    TestItem("Conjured Mana Cake", 0, 2, {
        std::make_pair(-1, 0),
    });

    TestItem("Conjured Mana Cake", 0, 1, {
        std::make_pair(-1, 0),
    });


    return 0;
}