#pragma once
#include "QualityUpdater.h"
class AgedBrieUpdater :
	public QualityUpdater
{
public:
	AgedBrieUpdater();
	~AgedBrieUpdater();
	void operator ()(Item& item) const override;
};

