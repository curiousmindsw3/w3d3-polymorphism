#pragma once
#include "QualityUpdater.h"
class DefaultUpdater :
	public QualityUpdater
{
public:
	DefaultUpdater();
	~DefaultUpdater();
	void operator () (Item& item) const override;
};

