#include "AgedBrieUpdater.h"



AgedBrieUpdater::AgedBrieUpdater()
{
}


AgedBrieUpdater::~AgedBrieUpdater()
{
}

void AgedBrieUpdater::operator()(Item & item) const
{
	item.Update(item.GetSellIn(), std::min(50, item.GetQuality() + 1));
}
