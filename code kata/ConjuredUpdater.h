#pragma once
#include "QualityUpdater.h"
class ConjuredUpdater :
	public QualityUpdater
{
public:
	ConjuredUpdater();
	~ConjuredUpdater();
	void operator () (Item& item) const override;
};

