#include "ConjuredUpdater.h"



ConjuredUpdater::ConjuredUpdater()
{
}


ConjuredUpdater::~ConjuredUpdater()
{
}

void ConjuredUpdater::operator()(Item & item) const
{
	item.Update(item.GetSellIn(), std::max(item.GetQuality() - 2, 0));
	if (item.GetSellIn() <= 0)
	{
		item.Update(item.GetSellIn(), std::max(item.GetQuality() - 2, 0));
	}

}
