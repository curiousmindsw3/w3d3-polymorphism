#include "DefaultUpdater.h"



DefaultUpdater::DefaultUpdater()
{
}


DefaultUpdater::~DefaultUpdater()
{
}

void DefaultUpdater::operator()(Item & item) const
{
	item.Update(item.GetSellIn(), std::max(item.GetQuality() - 1, 0));
}
