#include<iostream>
#include"IInterogate.h"
#include"IParticipation.h"
#include"IMedals.h"
#include"IAthlete.h"
#include"Swimmer.h"
#include"Runner.h"
#include"TennisPlayer.h"
#include <vector>
void printAthletes(const IInterogate *obj) {
	obj->Interogate();

}
void printAthletes(const IMedals * obj) {
	std::cout <<" and I have "<< obj->NumMedals() << " medals";
}
void printAthletes(const IParticipation * obj) {
	//std::cout <<" and "<< obj->Participation() <<" participations "<< std::endl;
	std::vector<char> vect;
	std::cout << " and i participated to ";
	int temp = obj->Participation();
	while (temp!=0) {
		vect.push_back(temp % 2);
		temp = temp / 2;
	}

	for (size_t i = 0; i < vect.size();i++) {
		if (vect[i] == 1) {
			switch (i)
			{
			case 0:
				std::cout << "Swimm_50m ";
				break;
			case 1:
				std::cout << "Swimm_100m ";
				break;
			case 2:
				std::cout << "Run_50 m ";
				break;
			case 3:
				std::cout << "Run_100 m ";
				break;
			case 4:
				std::cout << "Tenni_singles ";
				break;
			case 5:
				std::cout << "Tennis_doubles ";
				break;
			default:
				break;
			}
		}
	}
	std::cout << std::endl;
}
void PrintAthletes(const IAthlete** a, const int size) {
	for (int i = 0; i < size; i++) {
		printAthletes(a[i]->AsInterogate());
		printAthletes(a[i]->AsMedals());
		printAthletes(a[i]->AsParticipation());
	}
}
void main()
{
	const IAthlete** a = new const IAthlete*[3];
	a[0] = new Swimmer((char*)"S W I M M E R", (char*)"Country1", 3, 5);
	a[1] = new Runner((char*)" R U N N E R", (char*)"Country2", 12, 6);
	a[2] = new TennisPlayer((char*)"T E N N I S", (char*)"Country3", 32, 6);

	PrintAthletes(a, 3);
}