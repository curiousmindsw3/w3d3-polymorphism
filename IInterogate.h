#pragma once

class IInterogate
{
public:
	virtual ~IInterogate() {};
	virtual void Interogate() const = 0;
};

