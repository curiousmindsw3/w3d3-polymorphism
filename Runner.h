#pragma once
#include<iostream>
#include"IInterogate.h"
#include"IParticipation.h"
#include"IMedals.h"
#include"IAthlete.h"
class Runner:public IAthlete, public IInterogate, public IMedals, public IParticipation
{
	char *name, *country;
	int numMedals, disciplines;
public:
	Runner(char* name, char* country, int num, int disciplines)
		:name(name), country(country), numMedals(num), disciplines(disciplines)
	{}
	const IInterogate*  AsInterogate() const;
	const IParticipation* AsParticipation() const;
	const IMedals* AsMedals() const;
	int NumMedals() const;
	int Participation() const;
	void Interogate() const;
	~Runner();
};

