#pragma once

class IParticipation
{
public:
	enum Disciplines
	{
		Swimm_50m =1 <<0,
		Swimm_100m =1 << 1,
		Running_50m = 1 <<2,
		Running_100m = 1 <<3,
		Tennis_singles = 1 <<4,
		Tennis_doubles = 1 <<5
	};
	virtual int Participation() const=0;
	virtual ~IParticipation() {}
};

