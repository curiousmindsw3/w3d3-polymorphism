#include "Runner.h"

const IInterogate * Runner::AsInterogate() const
{
	return static_cast<const IInterogate*>(this);//!!
}

const IParticipation * Runner::AsParticipation() const
{
	return static_cast<const IParticipation*>(this);//!!
}

const IMedals * Runner::AsMedals() const
{
	return static_cast<const IMedals*>(this);//!!
}

int Runner::NumMedals() const
{
	return numMedals;
}

int Runner::Participation() const
{
	return disciplines;
}

void Runner::Interogate() const
{
	std::cout << "My name is " << name << ", I am from " << country;
}

Runner::~Runner()
{
}
